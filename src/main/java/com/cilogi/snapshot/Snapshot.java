// Copyright (c) 2015 Cilogi. All Rights Reserved.
//
// File:        Snapshot.java  (30/01/15)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.snapshot;

import com.cilogi.util.IOUtil;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.TimeUnit;


public class Snapshot {
    static final Logger LOG = LoggerFactory.getLogger(Snapshot.class);

    private static final File root = new File("c:\\tmp\\screenshots");

    public Snapshot() {

    }

    private static byte[] clip(int width, byte[] png) throws IOException {
        try (InputStream is = new ByteArrayInputStream(png)) {
            BufferedImage image = ImageIO.read(is);
            BufferedImage clipped = image.getSubimage(0, 0, width, image.getHeight());
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(clipped, "png", os);
            os.close();
            return os.toByteArray();
        }
    }

    private static void toFile(WebDriver driver, String name) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(root, name));
    }

    private static void toFileClipped(WebDriver driver, String name, int width) throws IOException {
        byte[] data = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        byte[] clipped = clip(width, data);
        IOUtil.storeBytes(clipped, new File(root, name));
    }

    public static void main(String[] args) {
        WebDriver driver = null;
        try {
            driver = new ChromeDriver();
            driver.manage().window().setPosition(new Point(0, 0));
            driver.manage().window().setSize(new Dimension(340, 700));
            driver.manage().timeouts().pageLoadTimeout(10L, TimeUnit.SECONDS);

            driver.get("http://sites.cilogi.com/site/botanics/index.html");

            //WebDriverWait wait = new WebDriverWait(driver, 10L);
            //wait.Until(d => d.FindElement(By.Id("Id_Your_UIElement"));
            try { Thread.sleep(2000L); } catch (InterruptedException e) {}

            //toFile(driver, "home.png");
            toFileClipped(driver, "home.png", 340);

            LOG.info("Created screen shot");
        } catch (Exception e) {
            LOG.error("oops", e);
        } finally {
            if (driver != null) {
                driver.quit();
            }
        }
    }
}
